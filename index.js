var requestify = require('requestify');
var cheerio = require('cheerio');

var $link = 'https://www.sucamec.gob.pe/poligono/faces/aplicacion/turTurno/ProgramarTurno.xhtml';

/*var $options = {
	    headers: {*/
	        //"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
/*	        "Accept-Encoding": "gzip, deflate, br",
	        "Accept-Language": "es-419,es;q=0.9,en;q=0.8",
	        "Cache-Control": "max-age=0",
	        "Connection": "keep-alive",
	        "Host": "www.sucamec.gob.pe",
	        "Upgrade-Insecure-Requests": "1",
	        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36"
	    },
	    cookies: {
	        "JSESSIONID": $session
	    },
		dataType: "json"
	};*/

requestify.get($link)
.then(function(response1) {
      // Get the response body (JSON parsed or jQuery object for XMLs)
	var $session=response1.headers['set-cookie'];
	console.log($session);
	$session =$session[0].split(";")[0].substring(11);
	console.log($session);
	//console.log(response.body);

	var $ = cheerio.load(response1.body);
	//console.log($);
	var $ViewState = '';
	$('#crearForm input').each(function(i, elem) {
		if($(this)[0].attribs.name == 'javax.faces.ViewState'){
			$ViewState = $(this)[0].attribs.value;
		}
	});
	console.log($ViewState);

	var $data = {
	    "javax.faces.partial.ajax": "true",
	    "javax.faces.source": "crearForm:sedeId",
	    "javax.faces.partial.execute": "crearForm:sedeId",
	    "javax.faces.partial.render": "crearForm",
	    "javax.faces.behavior.event": "change",
	    "javax.faces.partial.event": "change",
	    "crearForm:sedeId_focus": "",
	    "crearForm:sedeId_input": "51",
	    "javax.faces.ViewState": $ViewState
	};
	var $options = {
	    headers: {
	        "Accept": "application/xml, text/xml, */*; q=0.01",
	        //"Accept-Encoding": "gzip, deflate, br",
	        //"Accept-Language": "es-419,es;q=0.9,en;q=0.8",
	        "Connection": "keep-alive",
	        "Content-Length": "337",
	        //"Cache-Control": "max-age=0",
	        "Content-Type": "application/x-www-form-urlencoded",
	        //"Faces-Request": "partial/ajax",
	        "Host": "www.sucamec.gob.pe",
	        "Origin": "https://www.sucamec.gob.pe",
	        "Referer": "https://www.sucamec.gob.pe/poligono/faces/aplicacion/turTurno/ProgramarTurno.xhtml",
	        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Postman/4.10.3 Chrome/53.0.2785.143 Electron/1.4.12 Safari/537.36",
	        "X-Requested-With": "XMLHttpRequest"
	    },
	    cookies: {
	        "JSESSIONID": $session
	    },
		dataType: "json"
	};

	requestify.post($link, $data, $options)
	.then(function(response) {
	    // Get the response body
	    //console.log(response.getHeaders());
	    console.log("RESPUESTA");
	    console.log(response.getBody());
	    //console.log(response);
	});

}
);